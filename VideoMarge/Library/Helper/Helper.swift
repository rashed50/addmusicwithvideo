//
//  Helper.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-19.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI

let directoryName : String = "AllDocuments"
let tempDirectoryName : String = "tempDocuments"
let tempOCRDirectoryName : String = "ocrTempDocuments"

@objc class Helper: NSObject {
   
    
      static func sharePressed(tabbar : UITabBarController,nextTabIndex: Int, view: UIViewController, afile: FileInfo)
          {
             let alertController = UIAlertController(title: "Choose", message: "", preferredStyle: .actionSheet)
             let fileurl = afile.url
             let action1 = UIAlertAction(title: "Set  Merge with Video", style: .default) { (action) in
                 
                //let filepath :String =   String(fileurl.path)
                     tabbar.selectedIndex = nextTabIndex
                 self.setMergeAudioFile(aFileUrl: afile.url)
                     
             }
             let action2 = UIAlertAction(title: "Delete File", style: .default) { (action) in
                 
                     do {
                         try FileManager.default.removeItem(atPath: fileurl.path)
                          
                        } catch (let er) {
                            print(er)
                        }
             }
             let action3 = UIAlertAction(title: "Facebook", style: .default) { (action) in
                   print("Default is pressed.....")
               }
             let action4 = UIAlertAction(title: "Twitter", style: .default) { (action) in
                   print("Cancel is pressed......")
               }
             let action5 = UIAlertAction(title: "Email", style: .default) { (action) in
                   self.sendEmail(viewCon:view, fileUrl: fileurl ,operationType: 0)
               }
             let action6 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
             }
             
             if(afile.type == M4A_File_TYPE){
              alertController.addAction(action1)
              alertController.addAction(action2)
             // alertController.addAction(action3)
             // alertController.addAction(action4)
              alertController.addAction(action5)
              alertController.addAction(action6)
             }
             else {
                 alertController.addAction(action2)
                 alertController.addAction(action6)
             }
             if let popoverController = alertController.popoverPresentationController {
                     popoverController.sourceView = view.view
                     popoverController.sourceRect = CGRect(x: view.view.bounds.midX, y: view.view.bounds.midY, width: 0, height: 0)
                     popoverController.permittedArrowDirections = []
                   }
         view.present(alertController, animated: true, completion: nil)
      }
    
  @objc  static func showMessageView(title: String, message: String, view: UIViewController){
        
                    let alert = UIAlertController(title: title , message: message , preferredStyle: .alert)
                                   alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                         
                                   }))
                    view.present(alert, animated: true, completion: nil)
        
    }
    @objc
    static func sendEmail(viewCon:UIViewController, fileUrl: URL?,operationType:Int) {
           
           if MFMailComposeViewController.canSendMail() {
               
               let mail = MFMailComposeViewController()
               mail.mailComposeDelegate = viewCon as? MFMailComposeViewControllerDelegate
              
               if(operationType == 0){  // action sheet action
                   let attachmentData = NSData(contentsOfFile: fileUrl!.path)
                   let fileName : String = String(fileUrl!.absoluteString.split(separator: "/").last!)
                   let mimetype =  self.MimeTypefrom(filename: fileName)
                   mail.addAttachmentData(attachmentData! as Data, mimeType: mimetype, fileName: fileName)
                   mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
               }
               else if(operationType == 1) // user feedback and suggestion
               {
                   mail.setToRecipients(["rashed084050@gmail.com"])
                   mail.title = "User Feedback and Suggestion"
                   mail.setMessageBody("<p>Your Feedback and Suggestions is Our Motivation</p>", isHTML: true)
               }
               else if(operationType == 2) // Tell A Friend to share this app
               {
                   
                   mail.title = "Add Music with Videos"
                   mail.setMessageBody("https://apps.apple.com/us/app/add-music-with-video-sound/id1187313137", isHTML: true)
               }
               
               viewCon.present(mail, animated: true, completion: {
                   
                   print("test ")
                   
               })
           } else {
                
           }
       }
     
    

 
    static func MimeTypefrom(filename:String)-> String{
        
        var mimeType : String = ""
        if ( filename == M4A_File_TYPE ) {
            mimeType =  "audio/m4a"
        } else if (filename == "png" ) {
            mimeType = "image/png";
        } else if (filename == "doc" ) {
            mimeType = "application/msword";
        } else if (filename == "ppt" ) {
            mimeType = "application/vnd.ms-powerpoint";
        } else if (filename == "html" ) {
            mimeType =  "text/html"
        } else if (filename == "pdf" ) {
            mimeType = "application/pdf";
        }
        
        return mimeType
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
     
    
    static func getBasePath( basePath:String ) -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: AnyObject = paths[0] as AnyObject
        let dataPath = documentsDirectory.appending("/\(basePath)")
        return dataPath
    }
    @objc
    static func createDirectory() {
        
        do {
            try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: tempDirectoryName), withIntermediateDirectories: false, attributes: nil)
            try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: directoryName), withIntermediateDirectories: false, attributes: nil)
            try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: tempOCRDirectoryName), withIntermediateDirectories: false, attributes: nil)
            
            
            
        } catch let error as NSError {
            print(error.localizedDescription);
            
        }
        
    }
    

  
    
    
     @objc
    static func getFilePathWithExtention(_ fileName: String) -> String {
        let basePath = self.getBasePath(basePath: directoryName)
        let filePath = "\(basePath)/\(fileName)"
        return filePath
    }
     
    
    
     @objc
    static func getFileSavingDestinationPath(_ fileName: String , fileExtention: String) -> String {
        
      //  let basePath = self.getBasePath(basePath: directoryName)
        let basePath = self.createTodayFolder();
        let filePath = "\(basePath)/\(fileName).\(fileExtention)"
        return filePath
    }
    
   @objc
    static func moveFileFromLocationToLocation(_ filePath: String!,toLocation: String! ) -> String {
          let fileManager = FileManager.default
    do {
        try fileManager.moveItem(at: URL(fileURLWithPath: filePath), to:URL(fileURLWithPath: toLocation))
        print("file temp destination \(toLocation!)");
    }
    catch (let e) {
        print("data wirting error is %@",e)
        
    }
    
    return toLocation
    }
    
     @objc
    static func getAllFile() -> NSMutableDictionary {
        
        let listDictionaryData :NSMutableDictionary = NSMutableDictionary()
        
        var allDirList = NSMutableArray()
        allDirList = self.getAllDirectores()
        
        for folder in allDirList {
            let folderName = String(folder as! String)
            var basePath = "";
            basePath = basePath.appendingFormat("%@/%@", directoryName,folder as! String)
            basePath = self.getBasePath(basePath: basePath)
            
            do {
                let fileList = try FileManager.default.contentsOfDirectory(atPath: basePath)
                var aFileList  = [FileInfo]()
                
                if fileList.count > 0
                {
                    for var count in 0..<fileList.count{
                        let aFile :FileInfo = self.getFileNameAndTypeFromFilePath(path: basePath + "/" + fileList[count] )
                        aFileList.append(aFile)
                    }
                    listDictionaryData.setObject(aFileList, forKey: folderName as NSCopying)
                }
                
            }catch {
                
            }
        }
        return listDictionaryData
    }
    
     @objc
    static  func getTodayDate( ) -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.YYYY"
        let  todayDateStr = formatter.string(from: date)
        return todayDateStr.isEmpty ? "" : todayDateStr
    }
    
     @objc
    static func createTodayFolder() -> String {
        
        var basePath = "";
        basePath = basePath.appendingFormat("%@/%@", directoryName,self.getTodayDate())
        basePath = self.getBasePath(basePath: basePath)
        
        do {
            
            try FileManager.default.createDirectory(atPath:basePath, withIntermediateDirectories: false, attributes: nil)
            print("success")
            
        } catch let error as NSError {
            print(error.localizedDescription);
            
        }
        return basePath;
        
    }
    
     
    @objc
    static func getSystemGenerateFileName() -> String {
         let date = Date(timeIntervalSince1970: 1234566)

          let dateFormatter = DateFormatter()
          dateFormatter.locale = Locale(identifier: "en_US")
          dateFormatter.dateStyle = .medium

          return dateFormatter.string(from: date)
        
    }
     
     @objc
    static func getAllDirectores() -> NSMutableArray {
        
        let allDirectoryList : NSMutableArray = []
        do {
            let dirList = try FileManager.default.contentsOfDirectory(atPath: self.getBasePath(basePath: directoryName))
            
            for abc in dirList {
                allDirectoryList.add(abc)
            }
        }catch let error as NSError {
            print("error at getAllDirectory() ==>> \(error.localizedDescription)");
        }
        return allDirectoryList
    }
    
  
    
   
  static func GetDocumentDirectory() -> URL {
   
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!//.appendingPathComponent("AllFiles")
        return   documentsPath ;
   }
   
    
    static func getFileNameAndTypeFromFilePath(path:String)-> (FileInfo)  {
        
                 var spitlist = [String]()
                   spitlist = path.components(separatedBy:"/")
                   var type = String()
                   type = spitlist.last!.components(separatedBy: ".").last!
                  let url : URL = URL.init(fileURLWithPath: path)
                    let afile = FileInfo.init(name: spitlist.last!, type: type, url:url )
            return afile
    }
 
    
   //
  // MARK: SET NAVIGATION BAR PROPERTY
  //
     @objc
    static func setNavigationBarProperty(navbar:UINavigationController, size: Int ,title:String){
        
        let ssize  = CGFloat(size)
        let attrs = [
            NSAttributedString.Key.foregroundColor:getTextColor(),NSAttributedString.Key.font: UIFont(name:getAppTextFontName(), size:ssize)!
         ]
        navbar.navigationBar.titleTextAttributes = attrs
        navbar.navigationBar.topItem?.title = title
         navbar.navigationBar.backgroundColor = getAppColor()
        navbar.navigationBar.barStyle = UIBarStyle.blackTranslucent
    }
    
    @objc
      static func setTabBarProperty(tabBar:UITabBarController){
        
        tabBar.tabBar.barTintColor = getAppColor()
        tabBar.tabBar.barStyle = UIBarStyle.black;
        tabBar.tabBar.tintColor = getTextColor()
      }

    
      @objc
      static  func getAppColor() -> UIColor {

             // Convert hex string to an integer
             let hexint = Int(self.intFromHexString(hexStr: "000000"))  //APP_HEXA_COlOR_CODE
             let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
             let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
             let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
 
             // Create color object, specifying alpha as well
             let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
             return color
         }
      @objc
      static  func getTextColor() -> UIColor {

                // Convert hex string to an integer
                let hexint = Int(self.intFromHexString(hexStr: "ffffff"))
                let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                let blue = CGFloat((hexint & 0xff) >> 0) / 255.0

                // Create color object, specifying alpha as well
                let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                return color
            }
      static func intFromHexString(hexStr: String) -> UInt32 {
             var hexInt: UInt32 = 0
             // Create scanner
             let scanner: Scanner = Scanner(string: hexStr)
             // Tell scanner to skip the # character
             scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
             // Scan hex value
             scanner.scanHexInt32(&hexInt)
             return hexInt
         }
    private static func getAppTextFontName()-> String{
        
      //  return "HelveticaNeue-Bold"
         return "MarkerFelt-Wide"
      // return  "Georgia-Bold"
    }
      
    @objc  static func SetButtonUIProperty(button: UIButton,title:String,fontSize:Float){
          
          let fontSize = CGFloat(fontSize)
          button.backgroundColor = getAppColor()//  C80000
          if(title != ""){
              button.setTitle(title, for: .normal)
          }
          button.setTitleColor(getTextColor(), for: .normal)
         button.titleLabel!.font = UIFont.init(name:getAppTextFontName()  , size:fontSize)
        button.layer.cornerRadius = 10
           
      }
    
    
    

    
   //
  // MARK: USERDEAFULT PREFERENCE DATA
  //
       @objc
       static func setAllowsAudioCropping(onOff:Bool){
           
           let defaults = UserDefaults.standard
           defaults.set(onOff, forKey: "AudioCropping")
       }
       @objc
       static func getAllowsAudioCropping()->Bool{
              
            let defaults = UserDefaults.standard
    
           if (defaults.object(forKey: "AudioCropping") != nil) {
               
               let autoOnOff : Bool =  defaults.object(forKey: "AudioCropping") as! Bool
                      return autoOnOff
              }
           return true
       }
       
       @objc
       static func setRecordingDurationLimit(duration:Int){
              
              let defaults = UserDefaults.standard
                defaults.set(String(duration), forKey: "RecordDuration")
        }
        @objc
        static func getRecordingDurationLimit()->Float{
             //return 60.0
              let defaults = UserDefaults.standard
              if (defaults.object(forKey: "RecordDuration") != nil) {
                  
                  let recordDuration : String =  defaults.object(forKey: "RecordDuration") as! String
                  return  Float.init(recordDuration)!
                 }
              return 60.0
          }
       
    
      @objc
            static func setVideoToAudioConversionFormat(formatText:String){
                
                  let defaults = UserDefaults.standard
                  defaults.set(formatText, forKey: "AudioFormat")
            }
            @objc
            static func getVideoToAudioConversionFormat()->String{
                      
                   let defaults = UserDefaults.standard
                   if (defaults.object(forKey: "AudioFormat") != nil) {
                        return defaults.object(forKey: "AudioFormat") as! String
                      }
                   return "m4a"
               }
       @objc
       static func setVoiceReocrderHighlitedTextColor(color:String){
           
             let defaults = UserDefaults.standard
             defaults.set(color, forKey: "HighlitedTextColor")
       }
       @objc
       static func getVoiceReocrderHighlitedTextColor()->String{
                 
              let defaults = UserDefaults.standard
              if (defaults.object(forKey: "HighlitedTextColor") != nil) {
                   return defaults.object(forKey: "HighlitedTextColor") as! String
                 }
              return ""
          }
    
     @objc
          static func setMergeAudioFile(aFileUrl:URL){
              
                let defaults = UserDefaults.standard
                defaults.set(aFileUrl.absoluteString, forKey: "MerageAudioFile")
          }
     @objc
          static func getMergeAudioFile()->String{
                    
                 let defaults = UserDefaults.standard
                 if (defaults.object(forKey: "MerageAudioFile") != nil) {
                      return defaults.object(forKey: "MerageAudioFile") as! String
                    }
                 return ""
         }
    
    @objc
         static func setFirstVideoFile(aFileUrl:URL){
             
               let defaults = UserDefaults.standard
               defaults.set(aFileUrl.absoluteString, forKey: "FirstVideo")
         }
    @objc
         static func getFirstVideoFile()->String{
                   
                let defaults = UserDefaults.standard
                if (defaults.object(forKey: "FirstVideo") != nil) {
                     return defaults.object(forKey: "FirstVideo") as! String
                   }
                return ""
        }
    
    
    @objc
         static func setSecondVideoFile(aFileUrl:URL){
             
               let defaults = UserDefaults.standard
               defaults.set(aFileUrl.absoluteString, forKey: "SecondVideo")
         }
    @objc
         static func getSecondVideoFile()->String{
                   
                let defaults = UserDefaults.standard
                if (defaults.object(forKey: "SecondVideo") != nil) {
                     return defaults.object(forKey: "SecondVideo") as! String
                   }
                return ""
        }
    
}

