//
//  MergeAudioViewController.m
//  VideoMerge
//
//  Created by Rashedul Hoque on 25/4/20.
//  Copyright © 2020 SoftKnight. All rights reserved.
//

#import "MergeAudioViewController.h"
#import "SVProgressHUD.h"
#import "VideoMerge-Swift.h"

@interface MergeAudioViewController ()

@end

@implementation MergeAudioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Helper setFirstVideoFileWithAFileUrl:[NSURL URLWithString:@""]];
    [Helper setSecondVideoFileWithAFileUrl:[NSURL URLWithString:@""]];
    [Helper SetButtonUIPropertyWithButton:self.firstAssetBtn title:@"" fontSize:17];
    [Helper SetButtonUIPropertyWithButton:self.secondAssetBtn title:@"" fontSize:17];
    [Helper SetButtonUIPropertyWithButton:self.audioAssetBtn title:@"" fontSize:17];
    [Helper SetButtonUIPropertyWithButton:self.mergeBtn title:@"" fontSize:17];
    
}
 

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:@"Merge Music"];

}

#pragma UI Button Action

- (IBAction)LoadFirstVideo:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
    {
        [Helper showMessageViewWithTitle:@"Error" message:@"Album not Found"  view:self];
    }else{
        isSelectingAssetOne = TRUE;
        [self startMediaBrowserFromViewController: self
                                    usingDelegate: self];
    }
}
- (IBAction)LoadSecondVideo:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
    {
        
        [Helper showMessageViewWithTitle:@"Error" message:@"Album not Found" view:self];
        
    }else{
        isSelectingAssetOne = FALSE;
        [self startMediaBrowserFromViewController: self
                                    usingDelegate: self];
    }
}

- (IBAction)LoadAudio:(id)sender{
   
    
   //
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Options" message:@"" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *voiceRecord = [UIAlertAction actionWithTitle:@"Choose Voice Record" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            self.tabBarController.selectedIndex = 1;
                        }];
    UIAlertAction *music = [UIAlertAction actionWithTitle:@"Choose Music Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
         MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeMusic];
           mediaPicker.delegate = self;
           mediaPicker.allowsPickingMultipleItems = false;
         //  mediaPicker.prompt = @"Select Audio";
        [self presentViewController:mediaPicker animated:true completion:nil];
        
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:voiceRecord];
    [alert addAction:music];
    [self presentViewController:alert animated:YES completion:nil];
  
    
}




- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{
    NSArray * SelectedSong = [mediaItemCollection items];
    if([SelectedSong count]>0){
        MPMediaItem * SongItem = [SelectedSong objectAtIndex:0];
        NSURL *SongURL = [SongItem valueForProperty: MPMediaItemPropertyAssetURL];
        
        _audioAsset = [AVAsset assetWithURL:SongURL];
        NSLog(@"Audio Loaded");
       // [_music setTitle:@"Music Loaded" forState:UIControlStateNormal];
        [Helper showMessageViewWithTitle:@"Success" message:@"Audio Selection Done." view:self];
    }
    
    [self dismissViewControllerAnimated:true completion:nil];
}
- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    [self dismissViewControllerAnimated:true completion:nil];
}

// For responding to the user tapping Cancel.
- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI animated:true completion:^{}];
    return YES;
}
// For responding to the user accepting a newly-captured picture or movie
- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
   [self dismissViewControllerAnimated:true completion:nil];
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo) {
        if(isSelectingAssetOne){
                _firstAsset = [AVAsset assetWithURL:[info objectForKey:UIImagePickerControllerMediaURL]];
                [Helper setFirstVideoFileWithAFileUrl:[info objectForKey:UIImagePickerControllerMediaURL]];
                NSString *msg = @"Video File is Selected. \n Please Select Another One Video File";
                if (![[Helper getSecondVideoFile] isEqualToString:@""]) {
                    msg = @"Video File is Selected.";
                }
                [Helper showMessageViewWithTitle:@"Success" message:msg view:self];
           }
        else{
               _secondAsset = [AVAsset assetWithURL:[info objectForKey:UIImagePickerControllerMediaURL]];
               [Helper setSecondVideoFileWithAFileUrl:[info objectForKey:UIImagePickerControllerMediaURL]];
              [Helper showMessageViewWithTitle:@"Success" message:@"Second Video File is Selected" view:self];
             
        }
    }
}

- (IBAction)MergeAndSave:(id)sender{
    
     NSString *audioUrlString = [Helper getMergeAudioFile];
     NSString *fvUrlString = [Helper getFirstVideoFile];
     NSString *sfUrlString = [Helper getSecondVideoFile];
 
    if(_audioAsset != nil){
        audioUrlString = @"Audio Selected";
    }
    
    if([audioUrlString isEqualToString:@""]  || [fvUrlString isEqualToString:@""] || [sfUrlString isEqualToString:@""]){
         [Helper showMessageViewWithTitle:@"Error" message:@"Please Select Two Video & One Audio File" view:self];
         return ;
    }
     if(_audioAsset == nil){
          _audioAsset = [AVAsset assetWithURL:[NSURL URLWithString:audioUrlString]];
       }
      
      _firstAsset = [AVAsset assetWithURL:[NSURL URLWithString:fvUrlString]];
      _secondAsset = [AVAsset assetWithURL:[NSURL URLWithString:sfUrlString]];
    
    if(_firstAsset != nil && _secondAsset != nil && _audioAsset != nil)
    {
        [SVProgressHUD showWithStatus:@"Processing...." maskType:SVProgressHUDMaskTypeBlack];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                       {
                           
                           
                           // dispatch_sync(dispatch_get_main_queue(), ^(void){
                           
                           // });
                           
                           
                           
                           
                           //  [ActivityView startAnimating];
                           //Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
                           AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
                           
                           //VIDEO TRACK
                           AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
                           [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, _firstAsset.duration) ofTrack:[[_firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
                           
                           AVMutableCompositionTrack *secondTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
                           [secondTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, _secondAsset.duration) ofTrack:[[_secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:_firstAsset.duration error:nil];
                           
                           //AUDIO TRACK
                           if(_audioAsset!=nil)
                           {
                               AVMutableCompositionTrack *AudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
//                               [AudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, CMTimeAdd(firstAsset.duration, secondAsset.duration)) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
                              
                               //rashed
                               // for single video merge
                                  [AudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero,_firstAsset.duration) ofTrack:[[_audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
                               
                           }
                           
                           AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
                           MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeAdd(_firstAsset.duration, _secondAsset.duration));
                           
                           //FIXING ORIENTATION//
                           AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
                           AVAssetTrack *FirstAssetTrack = [[_firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
                           UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
                           BOOL  isFirstAssetPortrait_  = NO;
                           CGAffineTransform firstTransform = FirstAssetTrack.preferredTransform;
                           if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)  {FirstAssetOrientation_= UIImageOrientationRight; isFirstAssetPortrait_ = YES;}
                           if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {FirstAssetOrientation_ =  UIImageOrientationLeft; isFirstAssetPortrait_ = YES;}
                           if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {FirstAssetOrientation_ =  UIImageOrientationUp;}
                           if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {FirstAssetOrientation_ = UIImageOrientationDown;}
                           CGFloat FirstAssetScaleToFitRatio = 320.0/FirstAssetTrack.naturalSize.width;
                           if(isFirstAssetPortrait_){
                               FirstAssetScaleToFitRatio = 320.0/FirstAssetTrack.naturalSize.height;
                               CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
                               [FirstlayerInstruction setTransform:CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor) atTime:kCMTimeZero];
                           }else{
                               CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
                               [FirstlayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 160)) atTime:kCMTimeZero];
                           }
                           [FirstlayerInstruction setOpacity:0.0 atTime:_firstAsset.duration];
                           
                           AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:secondTrack];
                           AVAssetTrack *SecondAssetTrack = [[_secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
                           UIImageOrientation SecondAssetOrientation_  = UIImageOrientationUp;
                           BOOL  isSecondAssetPortrait_  = NO;
                           CGAffineTransform secondTransform = SecondAssetTrack.preferredTransform;
                           if(secondTransform.a == 0 && secondTransform.b == 1.0 && secondTransform.c == -1.0 && secondTransform.d == 0)  {SecondAssetOrientation_= UIImageOrientationRight; isSecondAssetPortrait_ = YES;}
                           if(secondTransform.a == 0 && secondTransform.b == -1.0 && secondTransform.c == 1.0 && secondTransform.d == 0)  {SecondAssetOrientation_ =  UIImageOrientationLeft; isSecondAssetPortrait_ = YES;}
                           if(secondTransform.a == 1.0 && secondTransform.b == 0 && secondTransform.c == 0 && secondTransform.d == 1.0)   {SecondAssetOrientation_ =  UIImageOrientationUp;}
                           if(secondTransform.a == -1.0 && secondTransform.b == 0 && secondTransform.c == 0 && secondTransform.d == -1.0) {SecondAssetOrientation_ = UIImageOrientationDown;}
                           CGFloat SecondAssetScaleToFitRatio = 320.0/SecondAssetTrack.naturalSize.width;
                           if(isSecondAssetPortrait_){
                               SecondAssetScaleToFitRatio = 320.0/SecondAssetTrack.naturalSize.height;
                               CGAffineTransform SecondAssetScaleFactor = CGAffineTransformMakeScale(SecondAssetScaleToFitRatio,SecondAssetScaleToFitRatio);
                               [SecondlayerInstruction setTransform:CGAffineTransformConcat(SecondAssetTrack.preferredTransform, SecondAssetScaleFactor) atTime:_firstAsset.duration];
                           }else{
                               ;
                               CGAffineTransform SecondAssetScaleFactor = CGAffineTransformMakeScale(SecondAssetScaleToFitRatio,SecondAssetScaleToFitRatio);
                               [SecondlayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(SecondAssetTrack.preferredTransform, SecondAssetScaleFactor),CGAffineTransformMakeTranslation(0, 160)) atTime:_firstAsset.duration];
                           }
                           
                           
                           MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,SecondlayerInstruction,nil];;
                           
                           AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
                           MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
                           MainCompositionInst.frameDuration = CMTimeMake(1, 30);
                           MainCompositionInst.renderSize = CGSizeMake(320.0, 480.0);
                           
                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                           NSString *documentsDirectory = [paths objectAtIndex:0];
                           NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
                           
                           NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                           
                           AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
                           exporter.outputURL=url;
                           exporter.outputFileType = AVFileTypeQuickTimeMovie;
                           exporter.videoComposition = MainCompositionInst;
                           exporter.shouldOptimizeForNetworkUse = YES;
                           [exporter exportAsynchronouslyWithCompletionHandler:^
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self exportDidFinish:exporter];
                                });
                            }];
                           
                       });
    }else
    {
        
       [Helper showMessageViewWithTitle:@"Error" message:@"Please Select Two Video & One Audio File" view:self];
    }
}
- (void)exportDidFinish:(AVAssetExportSession*)session
{
    if(session.status == AVAssetExportSessionStatusCompleted){
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:outputURL
                                        completionBlock:^(NSURL *assetURL, NSError *error){
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if (error) {
                                                     
                                                    [Helper showMessageViewWithTitle:@"Error" message:@"Failed to Merge Audio with Video" view:self];
                                                }else{
                                                    [Helper setFirstVideoFileWithAFileUrl:[NSURL URLWithString:@""]];
                                                    [Helper setSecondVideoFileWithAFileUrl:[NSURL URLWithString:@""]];
                                                    [Helper showMessageViewWithTitle:@"Success" message:@"New Video Saved in Phone Gallery" view:self];
                                                    
                                                }
                                                
                                            });
                                            
                                        }];
        }
    }
    
    _audioAsset = nil;
    _firstAsset = nil;
    _secondAsset = nil;
    [SVProgressHUD dismiss];
}

@end
