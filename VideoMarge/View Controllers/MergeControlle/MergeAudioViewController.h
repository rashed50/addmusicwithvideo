//
//  MergeAudioViewController.h
//  VideoMerge
//
//  Created by Rashedul Hoque on 25/4/20.
//  Copyright © 2020 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>

NS_ASSUME_NONNULL_BEGIN

@interface MergeAudioViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,MPMediaPickerControllerDelegate>{
    BOOL isSelectingAssetOne;
  
    
}

@property(nonatomic,retain)AVAsset* firstAsset;
@property(nonatomic,retain)AVAsset* secondAsset;
@property(nonatomic,retain)AVAsset* audioAsset;
@property (weak, nonatomic) IBOutlet UIButton *firstAssetBtn;

@property (weak, nonatomic) IBOutlet UIButton *secondAssetBtn;
@property (weak, nonatomic) IBOutlet UIButton *audioAssetBtn;

@property (weak, nonatomic) IBOutlet UIButton *mergeBtn;


- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate;
- (void)exportDidFinish:(AVAssetExportSession*)session;

- (IBAction)LoadFirstVideo:(id)sender;
- (IBAction)LoadSecondVideo:(id)sender;
- (IBAction)LoadAudio:(id)sender;
- (IBAction)MergeAndSave:(id)sender;
@end

NS_ASSUME_NONNULL_END
 
 
