//
//  ViewController.h
//  VideoMarge
//
//  Created by SoftKnight on 2/4/16.
//  Copyright © 2016 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface VideoController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;
@property (weak, nonatomic) IBOutlet UIButton *makeVideoButton;

@property (weak, nonatomic) IBOutlet UIButton *videoToAudioBtn;
@property (weak, nonatomic) IBOutlet UIButton *mergeVideoBtn;



- (IBAction)playVideoButtonAction:(id)sender;
- (IBAction)RecordButtonAction:(id)sender;
- (IBAction)MargeVideoButtonAction:(id)sender;
- (IBAction)ConvertVideoToMp3:(id)sender;

//For Opening UIImagePickerController
- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate;

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate;
- (void)video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo:(void*)contextInfo;

@end

