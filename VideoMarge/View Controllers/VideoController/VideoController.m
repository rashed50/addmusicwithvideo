//
//  ViewController.m
//  VideoMarge
//
//  Created by SoftKnight on 2/4/16.
//  Copyright © 2016 SoftKnight. All rights reserved.
//

#import "VideoController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "VideoMerge-Swift.h"
#import <AVKit/AVPlayerViewController.h>
#import "DPVideoMerger.h"
#import "SVProgressHUD.h"
#import "AssetPicker.h"


@interface VideoController ()



@end

@implementation VideoController
{
    NSString *recordingStatus;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Helper setTabBarPropertyWithTabBar:self.tabBarController];
    [Helper SetButtonUIPropertyWithButton:self.makeVideoButton title:@"" fontSize:17];
    [Helper SetButtonUIPropertyWithButton:self.mergeVideoBtn title:@"" fontSize:17];
    [Helper SetButtonUIPropertyWithButton:self.playVideoButton title:@"" fontSize:17];
    [Helper SetButtonUIPropertyWithButton:self.videoToAudioBtn title:@"" fontSize:17];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:@"Video Maker"];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)playVideoButtonAction:(id)sender {
     recordingStatus = @"play";
     [self startMediaBrowserFromViewController: self usingDelegate: self];
}

- (IBAction)RecordButtonAction:(id)sender {
    recordingStatus = @"afterRecord";
    [self startCameraControllerFromViewController: self
                                    usingDelegate: self];
}

-(void)openTLAsetPicker{
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          VideoVC *vc = [ main instantiateViewControllerWithIdentifier:@"videoVC"];
                          
    [self.navigationController pushViewController:vc animated:true];
  //  [self presentViewController:vc animated:true completion:{}];
}


- (IBAction)MargeVideoButtonAction:(id)sender {

   //  [self openTLAsetPicker];
 

    [AssetPicker showAssetPickerIn:self.navigationController
             maximumAllowedPhotos:0
             maximumAllowedVideos:5
                completionHandler:^(AssetPicker* picker, NSArray* assets)
    {
        
        [SVProgressHUD showWithStatus:@"Processing..."];
        NSLog(@"Assets --> %@", assets);
        NSMutableArray *fileURLs = [[NSMutableArray alloc] init];
        for (int i =0 ; i< assets.count; i++) {
            fileURLs[i] = [assets[i] objectForKey:@"APAssetContentsURL"];
        }
       
        [DPVideoMerger mergeVideosWithFileURLs:fileURLs completion:^(NSURL *mergedVideoFile, NSError *error) {
           
            [SVProgressHUD dismiss];
            if (error) {
                [Helper showMessageViewWithTitle:@"Error"  message:[NSString stringWithFormat:@"Could not merge videos: %@", [error localizedDescription]] view:self];
                return;
            }
            
            AVPlayerViewController *objAVPlayerVC = [[AVPlayerViewController alloc] init];
            objAVPlayerVC.player = [AVPlayer playerWithURL:mergedVideoFile];
            [self presentViewController:objAVPlayerVC animated:YES completion:^{
                [objAVPlayerVC.player play];
            }];
        }];

        [AssetPicker clearLocalCopiesForAssets];
    }
       cancelHandler:^(AssetPicker* picker)
    {
                NSLog(@"Cancelled.");
    }];
    
        /*
    
    
       [SVProgressHUD showWithStatus:@"Processing..."];
    
       NSString *filePath = [[NSBundle mainBundle] pathForResource:@"1" ofType:@"mp4"];
       NSURL *fileURL = [NSURL fileURLWithPath:filePath];
       NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"2" ofType:@"mp4"];
       NSURL *fileURL1 = [NSURL fileURLWithPath:filePath1];
       NSString *filePath2 = [[NSBundle mainBundle] pathForResource:@"3" ofType:@"MOV"];
       NSURL *fileURL2 = [NSURL fileURLWithPath:filePath2];
       NSString *filePath3 = [[NSBundle mainBundle] pathForResource:@"4" ofType:@"mp4"];
       NSURL *fileURL3 = [NSURL fileURLWithPath:filePath3];
       
       NSArray *fileURLs = @[fileURL, fileURL1,fileURL2,fileURL3];
       
       [DPVideoMerger mergeVideosWithFileURLs:fileURLs completion:^(NSURL *mergedVideoFile, NSError *error) {
           if (error) {
               NSString *errorMessage = [NSString stringWithFormat:@"Could not merge videos: %@", [error localizedDescription]];
               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
               [self presentViewController:alert animated:YES completion:nil];
               return;
           }
           [SVProgressHUD dismiss];
           AVPlayerViewController *objAVPlayerVC = [[AVPlayerViewController alloc] init];
           objAVPlayerVC.player = [AVPlayer playerWithURL:mergedVideoFile];
           [self presentViewController:objAVPlayerVC animated:YES completion:^{
               [objAVPlayerVC.player play];
           }];
       }];
    
    
    */
    
    
    
    /*
     
     // for swift add  pod 'DPVideoMerger-Swift
     import AVKit
     
     let fileURL = Bundle.main.url(forResource: "1", withExtension: "mp4")
     let fileURL1 = Bundle.main.url(forResource: "2", withExtension: "mp4")
     let fileURL2 = Bundle.main.url(forResource: "3", withExtension: "MOV")
     let fileURL3 = Bundle.main.url(forResource: "4", withExtension: "mp4")
     let fileURLs = [fileURL, fileURL1, fileURL2, fileURL3]
     
     DPVideoMerger().mergeVideos(withFileURLs: fileURLs as! [URL], completion: {(_ mergedVideoFile: URL?, _ error: Error?) -> Void in
         if error != nil {
             let errorMessage = "Could not merge videos: \(error?.localizedDescription ?? "error")"
             let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
             self.present(alert, animated: true) {() -> Void in }
             return
         }
         let objAVPlayerVC = AVPlayerViewController()
         objAVPlayerVC.player = AVPlayer(url: mergedVideoFile!)
         self.present(objAVPlayerVC, animated: true, completion: {() -> Void in
             objAVPlayerVC.player?.play()
         })
     })
    
     */
    
    
}

- (IBAction)ConvertVideoToMp3:(id)sender {
    
         recordingStatus = @"convertMp3";
        [self startMediaBrowserFromViewController: self usingDelegate: self];
    
}

/*
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    // This is the NSURL of the video object
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];

    NSLog(@"VideoURL = %@", videoURL);
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}
*/
 







- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate{
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI animated:true completion:^{}];
    return YES;
    
}
// For responding to the user tapping Cancel.
- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [self dismissViewControllerAnimated:true completion:^{}];
}

// For responding to the user accepting a newly-captured picture or movie
- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:false completion:^{}];
    
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef)mediaType, kUTTypeMovie, 0)
        == kCFCompareEqualTo) {
        
        NSString *moviePath = [[info objectForKey:
                                UIImagePickerControllerMediaURL] path];
       
         if( [recordingStatus isEqualToString: @"convertMp3"]){
                   
                 AVAsset   *firstAsset = [AVAsset assetWithURL:[info objectForKey:UIImagePickerControllerMediaURL]];
                
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:@"Please Input Your File Name" preferredStyle:UIAlertControllerStyleAlert];

             [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                 textField.placeholder = @"Enter File Name";
             }];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                     
                     
                                NSLog(@"inside dispatch async block main thread from main thread");
                                UITextField *fileName = alertController.textFields.firstObject;
                                if ([fileName.text isEqualToString:@""]) {
                                    fileName.text = [Helper getSystemGenerateFileName];
                                }
                                [self getAudioFromVideo:firstAsset fileName:fileName.text];
                        
                 }];
             
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                 
                            }];
             
                [alertController addAction:okAction];
                [alertController addAction:cancelAction];
            
                [self presentViewController:alertController animated:YES completion:nil];
            
               }
        else if ([recordingStatus isEqualToString:@"play"]) {
            
            /*
                     AVPlayerViewController* movie;
                     AVPlayer* player;
                     movie = [[AVPlayerViewController alloc] init];
                     movie.view.frame = self.view.bounds;
                     player = [[AVPlayer alloc] initWithURL:[info objectForKey:
                     UIImagePickerControllerMediaURL]];

                     [movie setPlayer:player];
                     [self.view addSubview:movie.view];

                     [player play];
            [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(myMovieFinishedCallback:)   name:AVPlayerItemDidPlayToEndTimeNotification object:player];
            
            */
                MPMoviePlayerViewController* theMovie =
                [[MPMoviePlayerViewController alloc] initWithContentURL: [info objectForKey:
                                                                  UIImagePickerControllerMediaURL]];
                [self presentViewController:theMovie animated:true completion:^{}];
                // Register for the playback finished notification
                [[NSNotificationCenter defaultCenter]
                 addObserver: self
                 selector: @selector(myMovieFinishedCallback:)
                 name: MPMoviePlayerPlaybackDidFinishNotification
                 object: theMovie];
            
            
        }
       
        else
        {
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                UISaveVideoAtPathToSavedPhotosAlbum (moviePath,self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
            }
        }
        
    }
}
// When the movie is done, release the controller.
-(void) myMovieFinishedCallback: (NSNotification*) aNotification
{
    [self dismissViewControllerAnimated:true completion:^{}];
    
    MPMoviePlayerController* theMovie = [aNotification object];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver: self
     name: MPMoviePlayerPlaybackDidFinishNotification
     object: theMovie];
    // Release the movie instance created in playMovieAtURL:
}

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    
    cameraUI.delegate = delegate;
    [controller presentViewController:cameraUI animated:true completion:^{}];
    return YES;
}

- (void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    
    if (error) {
       [Helper showMessageViewWithTitle:@"Error" message:@"Failed to Save Video in Phone Gallery" view:self];
    }else{
         
        [Helper showMessageViewWithTitle:@"Success" message:@"Your Video Save in Phone Gallery" view:self];
    }
}









#pragma USER DEFINE METHOD


-(void)getAudioFromVideo:(AVAsset*)myasset fileName:(NSString*)fileName {
    
    float startTime = 0;
    float endTime = 10;
    [super viewDidLoad];
   
    
    NSString *audioPath =   [Helper getFileSavingDestinationPath:fileName fileExtention:[Helper getVideoToAudioConversionFormat]];

    AVAssetExportSession *exportSession=[AVAssetExportSession exportSessionWithAsset:myasset presetName:AVAssetExportPresetAppleM4A];

    exportSession.outputURL=[NSURL fileURLWithPath:audioPath];
    exportSession.outputFileType=AVFileTypeAppleM4A;

    CMTime vocalStartMarker = CMTimeMake((int)(floor(startTime * 100)), 100);
    CMTime vocalEndMarker = CMTimeMake((int)(ceil(endTime * 100)), 100);

    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(vocalStartMarker, vocalEndMarker);
    exportSession.timeRange= exportTimeRange;
    if ([[NSFileManager defaultManager] fileExistsAtPath:audioPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:audioPath error:nil];
    }

    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if (exportSession.status==AVAssetExportSessionStatusFailed) {
            NSLog(@"failed");
        }
        else {
           // NSLog(@"AudioLocation : %@",audioPath);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tabBarController.selectedIndex = 1;
            });
        }
    }];
}


@end
