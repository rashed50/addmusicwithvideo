//
//  VideoVC.swift
//  VideoMerge
//
//  Created by Rashedul Hoque on 30/4/20.
//  Copyright © 2020 SoftKnight. All rights reserved.
//

import UIKit
import Photos
import MediaPlayer
import AVKit
import SwiftVideoGenerator


class VideoVC: UIViewController,TLPhotosPickerViewControllerDelegate,MPMediaPickerControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
       
      //  self.openAudioPicker()
        self.OpenPhotoGallery()
          
    }
    
    
  
    //
    // MARK TLPHOTO PICKER
    //
    func OpenPhotoGallery() {
         let viewController = TLPhotosPickerViewController()
         viewController.delegate = self
        // var configure = TLPhotosPickerConfigure()
         //configure.nibSet = (nibName: "CustomCell_Instagram", bundle: Bundle.main) // If you want use your custom cell..
         self.present(viewController, animated: true, completion: nil)
    }
    
   
    
    var selectedAssets = [TLPHAsset]()
  
    //TLPhotosPickerViewControllerDelegate
    func shouldDismissPhotoPicker(withTLPHAssets: [TLPHAsset]) -> Bool {
        // use selected order, fullresolution image
        self.selectedAssets = withTLPHAssets
    return true
    }
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        // if you want to used phasset.
        print("Item selected")
        print(withPHAssets)
    }
    func photoPickerDidCancel() {
        // cancel
    }
    func dismissComplete() {
        // picker viewcontroller dismiss completion
    }
    func canSelectAsset(phAsset: PHAsset) -> Bool {
        //Custom Rules & Display
        //You can decide in which case the selection of the cell could be forbidden.
        //print("can select asset ")
        return true
    }
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
        // exceed max selection
    }
    func handleNoAlbumPermissions(picker: TLPhotosPickerViewController) {
        // handle denied albums permissions case
    }
    func handleNoCameraPermissions(picker: TLPhotosPickerViewController) {
        // handle denied camera permissions case
    }
 

    //
    // MARK AUDIO PICKER
    //
    
    
    func openAudioPicker(){
        
        
        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.music)
        mediaPicker.delegate = self as MPMediaPickerControllerDelegate
        mediaPicker.allowsPickingMultipleItems = false
        mediaPicker.prompt = "Select Audio"
        self.present(mediaPicker, animated: true, completion: nil)
        
       }
       
       
       func mediaPicker(_ mediaPicker: MPMediaPickerController,
                        didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
         
           dismiss(animated: true) {
           let selectedSongs = mediaItemCollection.items
           guard let song = selectedSongs.first else { return }
           print(song.value(forProperty: MPMediaItemPropertyAssetURL) as? URL )
           //let url = song.value(forProperty: MPMediaItemPropertyAssetURL) as? URL
          // self.audioAsset = (url == nil) ? nil : AVAsset(url: url!)
         //  let title = (url == nil) ? "Asset Not Available" : "Asset Loaded"
          // let message = (url == nil) ? "Audio Not Loaded" : "Audio Loaded"
           
           
           Helper.showMessageView(title: "Ok", message: "Selected", view: self);
         }
       }

       func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
         dismiss(animated: true, completion: nil)
       }
    
    
    
    
    
    
    
    
    func MergeVideo() {
      
        
    }
    
    

}
