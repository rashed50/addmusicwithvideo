//
//  HistoryViewController.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-17.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI
import AVFoundation
import AVKit


class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,IQAudioRecorderViewControllerDelegate {
   
    

    
    var allDocumentInfoDic :NSMutableDictionary = NSMutableDictionary()
    
  
    let cellReuseIdentifier = "cellInfo"
    
    // don't forget to hook this up from the storyboard
    
    @IBOutlet var documentListTableView: UITableView!
    
    @IBOutlet weak var audioRecord: UIButton!
    
    @IBAction func aduioRecord(_ sender: Any) {
        
          let controller :IQAudioRecorderViewController = IQAudioRecorderViewController.init()
        
          // IQAudioRecorderViewController *controller = [[IQAudioRecorderViewController alloc] init];
           controller.delegate = self;
           controller.title = "Audio Recording";// textFieldTitle.text;
           controller.maximumRecordDuration = TimeInterval(Helper.getRecordingDurationLimit())
           controller.allowCropping = Helper.getAllowsAudioCropping() // switchAllowsCropping.on;
           controller.normalTintColor = UIColor.green // normalTintColor;
           controller.highlightedTintColor = UIColor.red //highlightedTintColor;
           controller.barStyle = UIBarStyle.black
         
           
              // [self presentBlurredAudioRecorderViewControllerAnimated:controller];
         
          self.presentAudioRecorderViewControllerAnimated(controller)
          
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        documentListTableView.delegate = self
        documentListTableView.dataSource = self
        documentListTableView.sectionHeaderHeight = 30
        documentListTableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
          self.documentListTableView.addGestureRecognizer(longPress)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:"File Manager")
        allDocumentInfoDic =  Helper.getAllFile()
        self.documentListTableView.reloadData()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        
    }
 //
/// MARK TABLE VIEW DELEGATE METHODS
//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (self.documentListTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier))!
        
        
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[indexPath.section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        let afile = listData[indexPath.row] as! FileInfo
        
         let iconView :UIImageView = cell.viewWithTag(10) as! UIImageView
         let nameLbl :UILabel = cell.viewWithTag(11) as! UILabel
         nameLbl.text = afile.name as String

        
        if(afile.type == M4A_File_TYPE)
           {
               iconView.image = UIImage.init(named: "voice-record-red.png")!
           }
           else{
               iconView.image = UIImage.init(named: "voice-record-red.png")!;
           }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
         return allSectionList[section] as? String
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        return allSectionList.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        return  listData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
        
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[indexPath.section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        let afile = listData[indexPath.row] as! FileInfo
        
       // var filePath : String = String(listData[indexPath.row] as! String)
       // filePath =  Helper.getFilePathWithExtention("\(sectionTitle)/\(filePath)")
        
       // let (afile) = Helper.getFileNameAndTypeFromFilePath(path: filePath)
        //print(filePath)
         
       // Helper.openSelectedFileBy(tabbar: self.tabBarController!,nextTabIndex:4 , view: self, afile: afile)
        self.playAudioFromURL(autioURL: afile.url)
        
    }

    
  @objc func handleLongPress(sender: UILongPressGestureRecognizer) {

      if sender.state == UIGestureRecognizer.State.began {
          let touchPoint = sender.location(in: documentListTableView)
          if documentListTableView.indexPathForRow(at: touchPoint) != nil {

            let indexpath : IndexPath = documentListTableView.indexPathForRow(at: touchPoint)!
            
                 let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
                   var sectionTitle = ""
                   sectionTitle = allSectionList[indexpath.section] as! String;
                   
                   let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
                   let afile = listData[indexpath.row] as! FileInfo
                Helper.sharePressed(tabbar: self.tabBarController!, nextTabIndex:2, view: self, afile: afile)
              print("Long press Pressed:)")
              
              
          }
      }


  }
  
    //
    // MARK USER DEFINE METHODS
    //
    
    func playAudioFromURL(autioURL:URL!) {
           /*
        var audioPlayer : AudioPlayerViewController
        if #available(iOS 13.0, *) {
              audioPlayer  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "audioPlayerViewController")
        } else {
            audioPlayer  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "audioPlayerViewController") as! AudioPlayerViewController

        }
        
        self.present(audioPlayer, animated: true, completion: nil)
        */
         
          let player = AVPlayer(url: autioURL!)
          let playerViewController = AVPlayerViewController()
          playerViewController.player = player
           self.present(playerViewController, animated: true)
          {
               playerViewController.player?.play()
           }
          
    }
   
    //
    // MARK AUDIORECORDERCONTROLLERDELGATE
    //
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
  
        controller.dismiss(animated: true, completion: {
            
            let alertController = UIAlertController(title: "File Name", message: "", preferredStyle: .alert)
            alertController.addTextField { textField in
                textField.placeholder = "Enter File Name"
                textField.textAlignment = .center
            }

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
             
            }

            let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in

                var fileName : String = alertController.textFields?.first?.text ?? ""
                if(fileName == ""){
                    fileName = Helper.getSystemGenerateFileName()
                    }
                let fileNewPath = Helper.getFileSavingDestinationPath(fileName, fileExtention: M4A_File_TYPE)
               _ = Helper.moveFileFromLocationToLocation(filePath, toLocation: fileNewPath)
                let aFile = Helper.getFileNameAndTypeFromFilePath(path: fileNewPath)
                let sectionTitleToday : String  = Helper.getTodayDate()
                
                let listData =  (self.allDocumentInfoDic.value(forKey: sectionTitleToday))
                if(listData != nil){
                    (listData as! NSArray).adding(aFile)
                    self.allDocumentInfoDic.setValue(listData, forKeyPath: sectionTitleToday)
                    DispatchQueue.main.async {
                   // self.documentListTableView.reloadSections(IndexSet(integersIn: 0...((listData as! NSArray).count-1)), with: UITableView.RowAnimation.top)
                    }
               }else {
                       var  newlistData = [FileInfo]()
                        newlistData.append(aFile)
                       self.allDocumentInfoDic.setValue(newlistData, forKeyPath: sectionTitleToday)
                    DispatchQueue.main.async {
                     //  self.documentListTableView.reloadSections(IndexSet(integersIn: 0...(newlistData.count-1)), with: UITableView.RowAnimation.top)
                    }
                }
            }
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        })
          
       }
    
    
}
 
extension HistoryViewController : MFMailComposeViewControllerDelegate{
    
    public func mailComposeController(_ controller: MFMailComposeViewController,
                                       didFinishWith result: MFMailComposeResult,
                                       error: Error?) {
         switch (result) {
         case .cancelled:
            print("Cancelled")
             controller.dismiss(animated: true, completion: nil)
         case .sent:
             print("Sent email")
             controller.dismiss(animated: true, completion: nil)
         case .failed:
             print("Failed")
             controller.dismiss(animated: true, completion: {
                
                 let sendMailErrorAlert = UIAlertController.init(title: "Failed",
                                                                 message: "Unable to send email. Please check your email and Internet Connection " +
                     "settings and try again.", preferredStyle: .alert)
                 sendMailErrorAlert.addAction(UIAlertAction.init(title: "OK",
                                                                 style: .default, handler: nil))
                 controller.present(sendMailErrorAlert, animated: true, completion: nil)
             })
         default:
             break;
         }
     }
}
