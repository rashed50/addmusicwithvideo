//
//  ViewController.m
//  IQAudioRecorderController Demo


#import "SettingViewController.h"
#import <IQAudioRecorderController/IQAudioRecorderViewController.h>
#import <IQAudioRecorderController/IQAudioCropperViewController.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ColorPickerTextField.h"
#import "IQAudioRecorderViewController.h"
#import "VideoMerge-Swift.h"
#import <MessageUI/MessageUI.h>
 

@interface SettingViewController ()<IQAudioRecorderViewControllerDelegate,IQAudioCropperViewControllerDelegate,ColorPickerTextFieldDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    IBOutlet UIBarButtonItem *buttonPlayAudio;
    IBOutlet UIBarButtonItem *barButtonCrop;
    NSString *audioFilePath;

    IBOutlet ColorPickerTextField *normalTintColorTextField;
    IBOutlet ColorPickerTextField *highlightedTintColorTextField;

    UIColor *normalTintColor;
    UIColor *highlightedTintColor;

    IBOutlet UITextField *textFieldTitle;
    IBOutlet UISwitch *switchDarkUserInterface;
    IBOutlet UISwitch *switchAllowsCropping;
    IBOutlet UISwitch *switchBlurEnabled;
    
    IBOutlet UILabel *labelMaxDuration;
    IBOutlet UIStepper *stepperMaxDuration;
}

@end

@implementation SettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    buttonPlayAudio.enabled = NO;
    barButtonCrop.enabled = NO;
    [switchAllowsCropping addTarget:self action:@selector(switchAllowsCroppingChanged:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar sizeToFit];
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction:)];
    toolbar.items = @[flexItem,doneItem];
    normalTintColorTextField.inputAccessoryView = highlightedTintColorTextField.inputAccessoryView = toolbar;
    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:@"Settings"];
    labelMaxDuration.text = [NSString stringWithFormat:@"%.0f", [Helper getRecordingDurationLimit]];
    stepperMaxDuration.value = (double) [Helper getRecordingDurationLimit];
    [switchAllowsCropping setOn:[Helper getAllowsAudioCropping]];
    highlightedTintColorTextField.text  = [Helper getVoiceReocrderHighlitedTextColor];
    highlightedTintColor  = highlightedTintColorTextField.selectedColor;
    
    
    
}


#pragma User Action Method

- (void)switchAllowsCroppingChanged:(UISwitch *)sender {
   [Helper setAllowsAudioCroppingOnOff:sender.isOn];
}
- (IBAction)switchThemeAction:(UISwitch *)sender
{
    if (sender.on)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.toolbar.barStyle = UIBarStyleBlack;
    }
    else
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
        self.navigationController.toolbar.barStyle = UIBarStyleDefault;
    }
}

- (IBAction)stepperDurationChanged:(UIStepper *)sender {
  
    labelMaxDuration.text = [NSString stringWithFormat:@"%.0f",sender.value];
    [Helper setRecordingDurationLimitWithDuration:sender.value];
}

#pragma Record

- (IBAction)recordAction:(UIBarButtonItem *)sender
{
    IQAudioRecorderViewController *controller = [[IQAudioRecorderViewController alloc] init];
    controller.delegate = self;
    controller.title = @"Audio Recording";// textFieldTitle.text;
    controller.maximumRecordDuration = stepperMaxDuration.value;
    controller.allowCropping = switchAllowsCropping.on;
    controller.normalTintColor = normalTintColor;
    controller.highlightedTintColor = highlightedTintColor;
    controller.barStyle = UIBarStyleBlack;
    
    if (switchDarkUserInterface.on)
    {
        controller.barStyle = UIBarStyleBlack;
    }
    else
    {
        controller.barStyle = UIBarStyleDefault;
    }

    if (switchBlurEnabled.on)
    {
        [self presentBlurredAudioRecorderViewControllerAnimated:controller];
    }
    else
    {
        [self presentAudioRecorderViewControllerAnimated:controller];
    }
}

-(void)audioRecorderController:(IQAudioRecorderViewController *)controller didFinishWithAudioAtPath:(NSString *)filePath
{
    audioFilePath = filePath;
    buttonPlayAudio.enabled = YES;
    barButtonCrop.enabled = YES;

    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)audioRecorderControllerDidCancel:(IQAudioRecorderViewController *)controller
{
    buttonPlayAudio.enabled = NO;
    barButtonCrop.enabled = NO;
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma Play

- (IBAction)playAction:(UIBarButtonItem *)sender
{
  //  MPMoviePlayerViewController *controller = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:audioFilePath]];
  //  [self presentMoviePlayerViewControllerAnimated:controller];
}

#pragma Crop

- (IBAction)cropAction:(UIBarButtonItem *)sender {

    IQAudioCropperViewController *controller = [[IQAudioCropperViewController alloc] initWithFilePath:audioFilePath];
    controller.delegate = self;
    controller.title = @"Crop";
    controller.normalTintColor = normalTintColor;
    controller.highlightedTintColor = highlightedTintColor;

    if (switchDarkUserInterface.on)
    {
        controller.barStyle = UIBarStyleBlack;
    }
    else
    {
        controller.barStyle = UIBarStyleDefault;
    }
    
    if (switchBlurEnabled.on)
    {
        [self presentBlurredAudioCropperViewControllerAnimated:controller];
    }
    else
    {
        [self presentAudioCropperViewControllerAnimated:controller];
    }
}

-(void)audioCropperController:(IQAudioCropperViewController *)controller didFinishWithAudioAtPath:(NSString *)filePath
{
    audioFilePath = filePath;
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)audioCropperControllerDidCancel:(IQAudioCropperViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}


#pragma Developer Info
- (IBAction)referToFriends:(id)sender {
  [Helper sendEmailWithViewCon:self fileUrl:nil operationType:2];

}


- (IBAction)developerApps:(id)sender {
    
          UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MyAppMain" bundle:nil];
          UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"pageContainerViewController"];
          vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
          [self.navigationController  pushViewController:vc animated:true];
    
    
}

- (IBAction)userFeedbackAndSuggession:(id)sender {
    
    [Helper sendEmailWithViewCon:self fileUrl:nil operationType:1];
}

#pragma Color Picker

-(void)colorPickerTextField:(nonnull ColorPickerTextField*)textField selectedColorAttributes:(nonnull NSDictionary<NSString*,id>*)colorAttributes
{
    if (textField.tag == 4)
    {
        normalTintColor = textField.selectedColor;
    }
    else if (textField.tag == 5)
    {
        [Helper setVoiceReocrderHighlitedTextColorWithColor:textField.text];
        highlightedTintColor = textField.selectedColor;
    }
}

#pragma TextField

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)doneAction:(UIBarButtonItem*)item
{
    [self.view endEditing:YES];
}


#pragma MAIL COMPOSER DELEGATE METHOD

- (void)mailComposeController:(MFMailComposeViewController*)controller
     didFinishWithResult:(MFMailComposeResult)result
                   error:(NSError*)error;
{
NSLog(@"Coming here");
if (result == MFMailComposeResultSent) {
   NSLog(@"Mail It's away!");
}
if (result == MFMailComposeResultFailed) {
   NSLog(@"Mail Error!");
}
if(result == MFMailComposeResultCancelled){
   NSLog(@"Mail Error!");

}
if(result == MFMailComposeResultSaved){
   NSLog(@"Mail Saved!");

}
[controller dismissViewControllerAnimated:YES completion:nil];
return;
}

@end
