import Foundation
import UIKit

class  MyPageViewController: UIPageViewController {
    fileprivate var myAllPublishApps: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        decoratePageControl()
        
        populateItems()
        if let firstViewController = myAllPublishApps.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    fileprivate func decoratePageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [MyPageViewController.self])
        pc.currentPageIndicatorTintColor = .orange
        pc.pageIndicatorTintColor = .gray
    }
    
    fileprivate func populateItems() {
        let appNameDetails = ["Kids Education Pre-School",
                              "Never Touch the Spikes",
                              "Barcode Toolbox-Scan,Create All QR & Data Matrix",
                              "QR Kit Pro Best Free app for Scan Solution",
                              "Smart document scanning",
                              "Photo Editor Makeup Camera & Gallery Images with amazing filter effects",
                              "Join Audio with Video Change video sound by new music",
                              "Barcode Maker Scan & Generate QR,Data matrix Code",
                              "QRCode Maker Scan & Generate Barcode,Data-martix",
                              "Edit Video Sound by new Audio",
                              "PDF Reader Master Search online pdf file,Read & Download",
                              "Run Tracker Walk or Run in Map and Keep History",
                              "Four in a row premium",
                            
        ]
        
        
        let appStoreLink = ["https://apps.apple.com/us/app/kids-education-pre-school/id1354440939",
                            "https://apps.apple.com/us/app/never-touch-the-spikes/id1075923082 ",
                           "https://apps.apple.com/us/app/barcode-toolbox-scan-create-all-qr-data-matrix/id1125807138",
                           "https://apps.apple.com/us/app/qr-kit-pro-best-free-app-for-scan-solution/id1067631975",
                           "https://apps.apple.com/us/app/smart-document-scanning/id1067569662",
                           "https://apps.apple.com/us/app/photo-editor-makeup-camera-gallery-images-amazing-filter/id1081080341",
                           "https://apps.apple.com/us/app/join-audio-with-video-change-video-sound-new-music/id1091900091",
                           "https://apps.apple.com/us/app/barcode-maker-scan-generate-qr-data-matrix-code/id1068482179",
                           "https://apps.apple.com/us/app/qrcode-maker-scan-generate-barcode-data-martix/id1016301698",
                           "https://apps.apple.com/us/app/edit-video-sound-by-new-audio/id1088660711",
                           "https://apps.apple.com/us/app/pdf-reader-master-search-online-pdf-file-read-download/id1030013377",
                           "https://apps.apple.com/us/app/run-tracker-walk-run-in-map-and-keep-history/id1107273736",
                           "https://apps.apple.com/us/app/four-in-a-row-premium/id1073932322",
        
        ]
        let backgroundColor:[UIColor] = [.lightGray, .purple, .green]
        
        for (index, t) in appNameDetails.enumerated() {
            let c = createCarouselItemControler(with: t, with: backgroundColor[0],appIconName:t,appStoreLink:appStoreLink[index])
            myAllPublishApps.append(c)
        }
    }
    
    fileprivate func createCarouselItemControler(with titleText: String?, with color: UIColor?, appIconName:String,appStoreLink:String) -> UIViewController {
        let c = UIViewController()
        c.view = ItemView(titleText: titleText, background: color, appIconName:appIconName,appStoreLink:appStoreLink)

        return c
    }
}

// MARK: - DataSource

extension MyPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = myAllPublishApps.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return myAllPublishApps.last
        }
        
        guard myAllPublishApps.count > previousIndex else {
            return nil
        }
        
        return myAllPublishApps[previousIndex]
    }
    
    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = myAllPublishApps.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        guard myAllPublishApps.count != nextIndex else {
            return myAllPublishApps.first
        }
        
        guard myAllPublishApps.count > nextIndex else {
            return nil
        }
        
        return myAllPublishApps[nextIndex]
    }
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return myAllPublishApps.count
    }
    
    func presentationIndex(for _: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = myAllPublishApps.firstIndex(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
}
