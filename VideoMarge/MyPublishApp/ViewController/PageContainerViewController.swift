import UIKit

class PageContainerViewController: UIViewController {

    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
      
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
       //   Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Developers  Apps")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Developer Apps"
    }
    
    @IBAction func onButtonNextTap(_ sender: Any) {
        
    }
}

